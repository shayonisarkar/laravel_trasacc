<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;
use Excel;
use App\ImportCsv;

class importcsv extends Controller{
	public function import(){

		Storage::disk('local')->put('tbl_member_account.csv','Contents');
 		Excel::load('tbl_member_account.csv', function($reader) {
    	$results = $reader->toArray();
    	foreach ($results as $key => $value) {
    		print_r($key);
    		echo '<br>';
    		print_r($value);
		});
 	}
 }
