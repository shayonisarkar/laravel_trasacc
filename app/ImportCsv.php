<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportCsv extends Model
{
    protected $table = 'importcsv';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_type', 'amount', 'date',
    ];
	public $timestamps = false;
    
}
